Summary 
(Da el resumen del Issue)

Steps to reproduce 
(Indica los pasos para reproducir el Bug)

What is the current behavior?

What is the expected behavior?